# IP address
import socket
import psutil

# System Uptime
import os
import time

# OLED Screen
from oled import OLED
from oled import Font
from oled import Graphics

# System temp
def temp():
  tFile = open('/sys/class/thermal/thermal_zone0/temp')
  temp = float(tFile.read())
  tempC = temp/1000
  tFile.close()

  return str("{0:.2f}".format(tempC))

# Ipv4 Address
def get_ip_addresses(family):
  for interface, snics in psutil.net_if_addrs().items():
    for snic in snics:
      if snic.family == family:
        yield (interface, snic.address)

# System uptime
def uptime():
  f = open( "/proc/uptime" )
  contents = f.read().split()
  f.close()

  total_seconds = float(contents[0])

  # Helper vars:
  MINUTE  = 60
  HOUR    = MINUTE * 60
  DAY     = HOUR * 24

  # Get the days, hours, etc:
  days    = int(total_seconds / DAY)
  hours   = int((total_seconds % DAY)/HOUR)
  minutes = int((total_seconds % HOUR)/MINUTE)

  # Format our return string ("N days, N hours, N minutes, N seconds")
  uptime = ""
  if days > 0:
    uptime += str(days) + "d "
  if len(uptime) > 0 or hours > 0:
    uptime += str(hours) + "h "
  if len(uptime) > 0 or minutes > 0:
    uptime += str(minutes) + "m"

  return str(uptime);

# Free memory (in megabytes)
def ram_info():
  free_stats = os.popen('free -m')
  i = 0

  # TODO: Find a better way to do this than using a while:true loop
  while 1:
    i = i + 1
    line = free_stats.readline()
    if i==2:
      return(line.split()[1:4])

# Connect to the display on /dev/i2c-1
dis = OLED(1)

# Start communication
dis.begin()
dis.clear()
dis.initialize()

# Set the orientation of our display module
#
# Setting to False marks the ribbon side as the top of the screen
# Setting to True flips the orientation and the screen 180°
#
#     ===           ________
#  =========       |        |
# | [False] |  vs. | [True] |
# |         |       ========
# |_________|         ===
#
dis.set_scan_direction(False)

# I'm doing this beacuse the example did it
dis.set_memory_addressing_mode(0)
dis.set_column_address(0, 127)
dis.set_page_address(0, 7)

# Capture stats
# --------------------------------------------------- #
system_uptime = str(uptime())
temperature = str(temp())
cpuload = str(psutil.cpu_percent(interval=None))

# IP address
ipv4s = list(get_ip_addresses(socket.AF_INET))
v4_tuple = ipv4s[-1]
ip = v4_tuple[-1]

# Free ram
free_stats = ram_info()
free_ram = str(free_stats[2]) + "mb"

# Time of day
#now = str(time.ctime())
now = time.strftime("%H:%M %a %Y/%m/%d")
# --------------------------------------------------- #

# Establish font, indent and line grid orientation on the screen

# Notes about this font setting:
# 21 chars across the screen is the limitation
# Longer strings wrap around the screen to the next line and do not honor indentation
# The following setting changes font scale: f.scale = 2
f = Font(1)

# Text placement
y_indent = int(2)
line1_x = int(2)
line2_x = int(12)
line3_x = int(27)
line4_x = int(41)
line5_x = int(54)

# Grid lines for text line 2
uptime_top_grid = int(23)
uptime_bottom_grid = int(38)
table_grid = int(50)
h_len = int(117)

# Creatively print all of our values to the screen
# Output grid: 127 px x 64 px (21 chars x 6 chars)
#
# |- Time Day Hour:Minute - |
# |- Uptime : Day Hour Min -|
# | ----------------------  |
# |- IP: IPv4 Address ----- |
# | ----------------------- |
# |-- Free * CPU * Temp --- |
# | ----------------------- |
# |- Ram mb | CPU Load | F  |

# Time
f.print_string(y_indent, line1_x, now)

# System Uptime
f.print_string(y_indent, line2_x, "Uptime: " + system_uptime)

# IPv4
Graphics.draw_line(1, uptime_top_grid, h_len, 0)
f.print_string(y_indent, line3_x, "IP: " + ip)

# Uptime line grid
Graphics.draw_line(1, uptime_bottom_grid, h_len, 0)

# Free ram + cpu + temp as table
f.print_string(y_indent, line4_x, "Free  * CPU  * Temp")
Graphics.draw_line(y_indent, table_grid, h_len, 0)
f.print_string(y_indent, line5_x, free_ram.ljust(6, ' ') + "| " + cpuload.ljust(5, ' ') + "|" + str(temperature).rjust(6, ' '))

# Send info to display and close file handle
dis.update()
dis.close()
