from oled import OLED

# Connect to the display on /dev/i2c-1
dis = OLED(1)

# Start communication
dis.begin()
dis.clear()
dis.initialize()

# Turn everything off
dis.deactivate_scroll()
dis.update()
dis.close()

